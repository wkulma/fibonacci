module Server

open Chiron
open Suave
open Suave.Filters
open Suave.Successful
open Suave.RequestErrors
open Fibonacci

let config = 
  { defaultConfig with bindings = [ HttpBinding.createSimple HTTP "0.0.0.0" 8080 ] }

type QueryParameter = UpperBound of int | LowerUpperBound of (int * int)

type Result = 
  { fibArray : string [] }
  static member ToJson(result : Result) =
    json { do! Json.write "Fibonacci sequence" result.fibArray }

let fibJsonArray (range : option<bigint> * bigint) = 
  { fibArray =
      range 
      |> go fib
      |> Seq.map string
      |> Seq.toArray }
  |> Json.serialize
  |> Json.format

let transformParamTuple (n : int * int) =
  fst n |> bigint |> Some, snd n |> bigint

let route (param : QueryParameter) =
  match param with
  | UpperBound param -> param |> bigint |> fun n -> (None, n)
  | LowerUpperBound param -> param |> transformParamTuple 
  |> fibJsonArray 
  |> OK

let app =
  choose [
    pathScan "/%d" (UpperBound >> route)
    pathScan "/%d/%d" (LowerUpperBound >> route)
    NOT_FOUND "Please specify an upper bound of the sequence"
  ]

let a = ""

[<EntryPoint>]
let main argv = 
  startWebServer config app
  0
